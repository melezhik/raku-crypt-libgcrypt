use NativeCall;
use Crypt::LibGcrypt;
use Crypt::LibGcrypt::CipherHandle;
use Crypt::LibGcrypt::Constants;

unit class Crypt::LibGcrypt::Cipher;

has Crypt::LibGcrypt::CipherHandle $.handle;
has Int $.algorithm;
has size_t $.keylen;
has size_t $.blklen;
has Bool $.bin;
has Bool $.debug is default(False);

sub gcry_cipher_algo_info(int32 $algo, int32 $what, Blob, size_t --> int32)
    is native(LIBGCRYPT) {}

sub gcry_cipher_open(Crypt::LibGcrypt::CipherHandle $handle is rw, int32 $algo,
                     int32 $mode, uint32 $flags --> int32)
    is native(LIBGCRYPT) {}

sub gcry_cipher_map_name(Str --> int32)
    is native(LIBGCRYPT) {}

sub gcry_cipher_algo_name(int32 --> Str)
    is native(LIBGCRYPT) {}

sub gcry_cipher_get_algo_keylen(int32 --> size_t)
    is native(LIBGCRYPT) {}

sub gcry_cipher_get_algo_blklen(int32 --> size_t)
    is native(LIBGCRYPT) {}

multi method available(Int:D $algorithm, Bool :$debug = False --> Bool:D)
{
    my $available = gcry_cipher_algo_info($algorithm, GCRYCTL_TEST_ALGO, buf8.new, 0);
    my $retcode   = $available == 0 ?? True !! False;

    dd sprintf("testing %s algo %03d, available %s", $algorithm.VAR.^name, $algorithm, $retcode) if $debug;

    return $retcode;
}

multi method available(Str:D $algorithm --> Bool:D)
{
    samewith gcry_cipher_map_name($algorithm) || return False;
}

multi submethod BUILD(Str:D :$algorithm, |opts)
{
    self.BUILD(algorithm => (gcry_cipher_map_name($algorithm)
               || die X::Crypt::LibGcrypt::BadAlgorithm.new(:$algorithm)), |opts)
}

multi submethod BUILD(Int:D :$!algorithm,
                :$mode is copy,
                :$key,
                :$iv,
                :$ctr,
                Bool :$!bin,
                Bool :$!debug = False,
                )
{
    die X::Crypt::LibGcrypt::BadAlgorithm.new(:$!algorithm)
        unless Crypt::LibGcrypt::Cipher.available($!algorithm, :$!debug);

    $!keylen = gcry_cipher_get_algo_keylen($!algorithm);
    $!blklen = gcry_cipher_get_algo_blklen($!algorithm);

    if $mode ~~ Str
    {
        $mode = Crypt::LibGcrypt::CipherMode::{sprintf("GCRY_CIPHER_MODE_%s", $mode.uc())}
                // die X::Crypt::LibGcrypt::BadMode.new(:$mode)
    }
    else
    {
        $mode //= $!blklen > 1 ?? Crypt::LibGcrypt::CipherMode::{'GCRY_CIPHER_MODE_CBC'}
                               !! Crypt::LibGcrypt::CipherMode::{'GCRY_CIPHER_MODE_STREAM'};
    }

    $!handle .= new;

    Crypt::LibGcrypt.check: gcry_cipher_open($!handle, $!algorithm, $mode, 0);

    self.setkey($_) with $key;
    self.setiv($_) with $iv;
    self.setctr($_) with $ctr;
}

multi method setkey(Blob:D $key where *.bytes == $!keylen) {
    Crypt::LibGcrypt.check: $!handle.setkey($key, $key.bytes)
}

multi method setkey(Blob:D $key where *.bytes < $!keylen)
{
    samewith Buf.new($key).append(0 xx $!keylen - $key.bytes)
}

multi method setkey(Blob:D $key where *.bytes > $!keylen)
{
    samewith Buf.new($key).reallocate($!keylen)
}

multi method setkey(Str:D $key) { samewith $key.encode }

multi method setiv(Blob:D $iv where *.bytes == $!blklen)
{
    Crypt::LibGcrypt.check: $!handle.setiv($iv, $iv.bytes)
}

multi method setiv(Blob:D $iv where *.bytes < $!blklen)
{
    samewith Buf.new($iv).append(0 xx $!blklen - $iv.bytes)
}

multi method setiv(Blob:D $iv where *.bytes > $!blklen)
{
    samewith Buf.new($iv).reallocate($!blklen)
}

multi method setiv(Str:D $iv) { samewith $iv.encode }

multi method setiv()
{
    samewith Buf.new(0 xx $!blklen)
}

#

multi method setctr(Blob:D $ctr where *.bytes == $!blklen)
{
    Crypt::LibGcrypt.check: $!handle.setctr($ctr, $ctr.bytes)
}

multi method setctr(Blob:D $ctr where *.bytes < $!blklen)
{
    samewith Buf.new($ctr).append(0 xx $!blklen - $ctr.bytes)
}

multi method setctr(Blob:D $ctr where *.bytes > $!blklen)
{
    samewith Buf.new($ctr).reallocate($!blklen)
}

multi method setctr(Str:D $ctr) { samewith $ctr.encode }

multi method setctr()
{
    samewith Buf.new(0 xx $!blklen)
}

submethod DESTROY()
{
    .close with $!handle;
    $!handle = Nil;
}

multi method encrypt(Blob:D $in where *.bytes %% $!blklen --> Blob)
{
    my $out = buf8.allocate($in.bytes);
    Crypt::LibGcrypt.check: $!handle.encrypt($out, $out.bytes, $in, $in.bytes);
    $out
}

multi method encrypt(Blob:D $in where !(*.bytes %% $!blklen))
{
    samewith Buf.new($in).append(0 xx $!blklen - $in.bytes % $!blklen)
}

multi method encrypt(Str:D $in --> Blob)
{
    samewith $in.encode
}

multi method decrypt(Blob:D $in, Bool :$bin = $!bin)
{
    my $out = buf8.allocate($in.bytes);
    Crypt::LibGcrypt.check: $!handle.decrypt($out, $out.bytes, $in, $in.bytes);
    return $out if $bin;
    my $bytes = $out.bytes;
    $bytes-- while $out[$bytes-1] == 0;
    $out.reallocate($bytes).decode
}

method name(--> Str:D)
{
    gcry_cipher_algo_name($!algorithm)
}

method reset()
{
    Crypt::LibGcrypt.check: $!handle.control(GCRYCTL_RESET, Pointer, 0);
    self
}
