#!/usr/bin/env raku

use Test;
use Crypt::LibGcrypt;

plan 1;

ok my $version = Crypt::LibGcrypt.version(), 'Get Version';
diag "libgcrypt version $version";

done-testing;
