use NativeCall;
use Crypt::LibGcrypt;
use Crypt::LibGcrypt::HashHandle;
use Crypt::LibGcrypt::Constants;

unit class Crypt::LibGcrypt::Hash;

has Crypt::LibGcrypt::HashHandle $.handle handles <reset>;
has Int $.length;

sub gcry_md_map_name(Str --> int32)
    is native(LIBGCRYPT) {}

sub gcry_md_get_algo_dlen(int32 --> int32)
    is native(LIBGCRYPT) {}

sub gcry_md_open(Crypt::LibGcrypt::HashHandle $handle is rw, int32 $algo, uint32 $flags
                 --> int32)
    is native(LIBGCRYPT) {}

sub gcry_md_algo_name(int32 --> Str)
    is native(LIBGCRYPT) {}

sub gcry_md_algo_info(int32 $algo, int32 $what, Pointer, Pointer --> int32)
    is native(LIBGCRYPT) {}

multi method available(Str:D $algorithm --> Bool)
{
    samewith gcry_md_map_name($algorithm)
}

multi method available(Int:D $algorithm --> Bool)
{
    gcry_md_algo_info($algorithm, GCRYCTL_TEST_ALGO, Pointer, Pointer) == 0
}

sub gcry_md_copy(Crypt::LibGcrypt::HashHandle $handle is rw, Crypt::LibGcrypt::HashHandle --> int32)
    is native(LIBGCRYPT) {}

multi submethod BUILD(Crypt::LibGcrypt::HashHandle:D :$!handle, Int:D :$!length) {}

multi submethod BUILD(Str:D :$algorithm, |opts)
{
    self.BUILD(algorithm => gcry_md_map_name($algorithm)
               || die X::Crypt::LibGcrypt::BadAlgorithm.new(:$algorithm), |opts)
}

multi submethod BUILD(Int:D :$algorithm,
                      :$key,
                      Bool :$hmac,
                      Bool :$bugemu1)
{
    my uint32 $flags = ($hmac    ?? GCRY_MD_FLAG_HMAC    !! 0)
                    +| ($bugemu1 ?? GCRY_MD_FLAG_BUGEMU1 !! 0);

    $!handle .= new;

    Crypt::LibGcrypt.check: gcry_md_open($!handle, $algorithm, $flags);

    $!length = gcry_md_get_algo_dlen($algorithm);

    Crypt::LibGcrypt.check($!handle.setkey($_)) with $key;
}

method clone(--> Crypt::LibGcrypt::Hash)
{
    my Crypt::LibGcrypt::HashHandle $handle .= new;
    Crypt::LibGcrypt.check: gcry_md_copy($handle, $!handle);
    self.bless(:$handle, :$!length)
}

submethod DESTROY()
{
    .close with $!handle;
    $!handle = Nil;
}

method algorithm(--> Crypt::LibGcrypt::MD) { Crypt::LibGcrypt::MD($!handle.algorithm) }

method name(--> Str:D) { gcry_md_algo_name($!handle.algorithm) }

multi method write(Any:U)
{
    self;
}

multi method write(Blob:D $buf --> Crypt::LibGcrypt::Hash)
{
    $!handle.write($buf, $buf.bytes); self
}

multi method write(Str:D $str --> Crypt::LibGcrypt::Hash)
{
    samewith $str.encode
}

method digest(Int $bytes = 0, Bool :$reset --> Blob:D)
{
    my $buf;
    if $!length == 0
    {
        die X::Crypt::LibGcrypt::ExtendedOutput.new(algorithm => $.algorithm)
            unless $bytes > 0;

        $buf = buf8.allocate($bytes);
        Crypt::LibGcrypt.check: $!handle.extract(0, $buf, $bytes);
    }
    else
    {
        $buf = Blob.new: ($!handle.read(0)
                          // die X::Crypt::LibGcrypt::Invalid.new)[^$.length];
    }
    $!handle.reset() if $reset;
    $buf
}

method hex(|opts --> Str:D)
{
    $.digest(|opts)».fmt("%02x").join
}

method dec(|opts --> Int:D)
{
    $.hex(|opts).parse-base(16)
}
