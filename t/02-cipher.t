#!/usr/bin/env raku

use Test;
use Crypt::LibGcrypt::Constants;
use Crypt::LibGcrypt::Cipher;
use Crypt::LibGcrypt::Random;

my $plaintext = 'The quick brown fox jumps over the lazy dog';
my $key = 'this4_#xxyh%%3hasd';

constant CIFERS = {
    "GCRY_CIPHER_IDEA"        => GCRY_CIPHER_IDEA,
    "GCRY_CIPHER_3DES"        => GCRY_CIPHER_3DES,
    "GCRY_CIPHER_CAST5"       => GCRY_CIPHER_CAST5,
    "GCRY_CIPHER_BLOWFISH"    => GCRY_CIPHER_BLOWFISH,
    "GCRY_CIPHER_AES"         => GCRY_CIPHER_AES,
    "GCRY_CIPHER_AES192"      => GCRY_CIPHER_AES192,
    "GCRY_CIPHER_AES256"      => GCRY_CIPHER_AES256,
    "GCRY_CIPHER_TWOFISH"     => GCRY_CIPHER_TWOFISH,
    "GCRY_CIPHER_ARCFOUR"     => GCRY_CIPHER_ARCFOUR,
    "GCRY_CIPHER_DES"         => GCRY_CIPHER_DES,
    "GCRY_CIPHER_TWOFISH128"  => GCRY_CIPHER_TWOFISH128,
    "GCRY_CIPHER_SERPENT128"  => GCRY_CIPHER_SERPENT128,
    "GCRY_CIPHER_SERPENT192"  => GCRY_CIPHER_SERPENT192,
    "GCRY_CIPHER_SERPENT256"  => GCRY_CIPHER_SERPENT256,
    "GCRY_CIPHER_RFC2268_40"  => GCRY_CIPHER_RFC2268_40,
    "GCRY_CIPHER_RFC2268_128" => GCRY_CIPHER_RFC2268_128,
    "GCRY_CIPHER_SEED"        => GCRY_CIPHER_SEED,
    "GCRY_CIPHER_CAMELLIA128" => GCRY_CIPHER_CAMELLIA128,
    "GCRY_CIPHER_CAMELLIA192" => GCRY_CIPHER_CAMELLIA192,
    "GCRY_CIPHER_CAMELLIA256" => GCRY_CIPHER_CAMELLIA256,
    "GCRY_CIPHER_SALSA20"     => GCRY_CIPHER_SALSA20,
    "GCRY_CIPHER_SALSA20R12"  => GCRY_CIPHER_SALSA20R12,
    "GCRY_CIPHER_GOST28147"   => GCRY_CIPHER_GOST28147,
    "GCRY_CIPHER_CHACHA20"    => GCRY_CIPHER_CHACHA20,
};

plan CIFERS.keys.elems + 1;

for CIFERS.kv -> $name, $algorithm
{
    subtest $name,
    {
        plan 5;

        if Crypt::LibGcrypt::Cipher.available($algorithm, :debug(True))
        {
            isa-ok my $obj = Crypt::LibGcrypt::Cipher.new(:$algorithm, :$key),
                Crypt::LibGcrypt::Cipher, 'open';

            ok my $encrypted = $obj.encrypt($plaintext), 'encrypt';

            ok $obj.reset, 'reset';

            ok my $decrypted = $obj.decrypt($encrypted), 'decrypt';

            is $decrypted, $plaintext, 'correct';
        }
        else
        {
            skip "Unknown algorithm $algorithm", 5;
        }
    }
}

subtest 'AES128-CTR',
{
    plan 3;

    if Crypt::LibGcrypt::Cipher.available(GCRY_CIPHER_AES, :debug(True))
    {
        my buf8 $ivbuf  = random(16);
        my buf8 $keybuf = buf8.new($key.encode).subbuf(0, 16);
        my buf8 $secret = buf8.new($plaintext.encode);

        ok my $encrypted = Crypt::LibGcrypt::Cipher.new(:algorithm(GCRY_CIPHER_AES), :key($keybuf), :mode('CTR'), :ctr($ivbuf)).encrypt($secret, :bin(True)), 'encrypt';
        ok my $decrypted = Crypt::LibGcrypt::Cipher.new(:algorithm(GCRY_CIPHER_AES), :key($keybuf), :mode('CTR'), :ctr($ivbuf)).decrypt($encrypted, :bin(True)), 'decrypt';

        is-deeply $decrypted.subbuf(0, $secret.bytes), $secret, 'correct';
    }
    else
    {
        skip-rest(sprintf("algorithm GCRY_CIPHER_AES (%d) is unavailable", GCRY_CIPHER_AES));
    }
}

done-testing;
